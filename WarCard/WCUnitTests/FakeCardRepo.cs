﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WCUnitTests
{
    public class FakeCardRepo
    {
        public static List<DataAccessLayer.Card> Cards()
        {
            var cards = new List<Card>
            {
                //common
                new Card
                {
                    CardName = "Angry Chicken",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Argent Squire",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Bloodsail Corsair",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Cogmaster",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Elven Archer",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Frost Shock",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Goldshire Footman",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Grimscale Oracle",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Hungry Crab",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Light's Justice",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Mana Wyrm",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Murloc Tidecaller",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Northshire Cleric",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Secretkeeper",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Shield Slam",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Stonetusk Boar",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Timber Wolf",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Undertaker",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Voidwalker",
                    CardType = "Common"
                },
                new Card
                {
                    CardName = "Webspinner",
                    CardType = "Common"
                },
                //Uncommon
                new Card
                {
                    CardName = "Amani Berserker",
                    CardType = "Uncommon"
                },
                new Card
                {
                    CardName = "Anodized Robo Cub",
                    CardType = "Uncommon"
                },
                new Card
                {
                    CardName = "Bluegill Warrior",
                    CardType = "Uncommon"
                },
                new Card
                {
                    CardName = "Glaivezooka",
                    CardType = "Uncommon"
                },
                new Card
                {
                    CardName = "Loot Hoarder",
                    CardType = "Uncommon"
                },
                new Card
                {
                    CardName = "Mad Scientist",
                    CardType = "Uncommon"
                },
                new Card
                {
                    CardName = "Murloc Tidehunter",
                    CardType = "Uncommon"
                },
                new Card
                {
                    CardName = "Snowhugger",
                    CardType = "Uncommon"
                },
                        
                //rare
                new Card
                {
                    CardName = "Aldor Peacekeeper",
                    CardType = "Rare"
                },
                new Card
                {
                    CardName = "Felguard", 
                    CardType = "Rare"
                },
                new Card
                {
                    CardName = "Powermace",
                    CardType = "Rare"
                },
                new Card
                {
                    CardName = "Soot Spewer",
                    CardType = "Rare"
                },
                //epic
                new Card
                {
                    CardName = "Dark Iron Dwarf",
                    CardType = "Epic"
                },
                new Card
                {
                    CardName = "Kezan Mystic",
                    CardType = "Epic"
                },
                new Card
                {
                    CardName = "Mechanical Yeti",
                    CardType = "Epic"
                },
                new Card
                {
                    CardName = "Twilight Drake",
                    CardType = "Epic"
                },
                //tier one
                new Card
                {
                    CardName = "Booty Bay Bodyguard",
                    CardType = "Tier1"
                },
                new Card
                {
                    CardName = "Doomguard",
                    CardType = "Tier1"
                },
                new Card
                {
                    CardName = "Siege Engine",
                    CardType = "Tier1"
                },
                new Card
                {
                    CardName = "Upgraded Repair Bot",
                    CardType = "Tier1"
                },
                        
                //tier two
                new Card
                {
                    CardName = "Healing Touch",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Antique Healbot",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Ancestral Healing",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Darkscale Healer",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Holy Light",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Lightwell",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Light of the Naaru",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Voodoo Doctor",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Tree of Life",
                    CardType = "Tier2"
                },
                new Card
                {
                    CardName = "Vitality Totem",
                    CardType = "Tier2"
                },
                        
                //tier three
                new Card
                {
                    CardName = "Arcane Intellect",
                    CardType = "Tier3"
                },
                new Card
                {
                    CardName = "Divine Favor",
                    CardType = "Tier3"
                },
                new Card
                {
                    CardName = "Far Sight",
                    CardType = "Tier3"
                },
                new Card
                {
                    CardName = "Mortal Coil",
                    CardType = "Tier3"
                },
                        
                //legendary
                new Card
                {
                    CardName = "Lord Jaraxxus",
                    CardType = "Legendary"
                },
                new Card
                {
                    CardName = "Dr. Boom",
                    CardType = "Legendary"
                }
            };
            return cards;
        }
    }
}
