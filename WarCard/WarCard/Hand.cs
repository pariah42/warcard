using System.Collections.Generic;
using System.Linq;
using WarCard.Cards;

namespace WarCard
{
    public class Hand : IHand
    {
        public Hand(List<ICard> cards)
        {
            Cards = cards;
        }

        public int CardCount { get { return Cards.Count; } }
        public List<ICard> Cards { get; private set; }

        public void AddCard(ICard card)
        {
            Cards.Add(card);
        }

        public ICard CardAtIndex(int index)
        {
            if (index < 0 || index >= Cards.Count)
            {
                return null;
            }
            return Cards.ElementAt(index);
        }
    }
}