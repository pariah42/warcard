using System;
using System.Collections.Generic;
using System.Linq;
using WarCard.Cards;
using WarCard.NPCs;

namespace WarCard
{
    public class Deck : IDeck
    {
        private List<ICard> _cards; 

        public Deck(ICardTypeCounts cardTypeCounts)
        {
            _cards = new List<ICard>(cardTypeCounts.Total);
            CardTypeCounts = cardTypeCounts;
        }

        public ICardTypeCounts CardTypeCounts { get; private set; }
        public int CardsInDeck { get { return _cards.Count; } }

        public void AddCard(ICard card)
        {
            _cards.Add(card);
        }

        public ICard DrawCard()
        {
            var removedCard = _cards.ElementAt(0);
            _cards.RemoveAt(0);
            return removedCard;
        }

        public List<ICard> DrawCards(int quantity)
        {
            List<ICard> removedCards;
            if (quantity >= _cards.Count)
            {
                removedCards = new List<ICard>(_cards);
                _cards.Clear();
                return removedCards;
            }
            removedCards = new List<ICard>();
            for (int i = 0; i < quantity; i++)
            {
                removedCards.Add(_cards.ElementAt(0));
                _cards.RemoveAt(0);
            }
            return removedCards;
        } 

        public void Shuffle()
        {
            Random rnd = new Random(DateTime.Now.Millisecond);
            int count = CardsInDeck;
            for (int i = 0; i < count; i++)
            {
                int nextRandom = rnd.Next(i, count);
                var temp = _cards[i];
                _cards[i] = _cards[nextRandom];
                _cards[nextRandom] = temp;
            }
        }
    }
}