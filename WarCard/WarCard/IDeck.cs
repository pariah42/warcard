using System.Collections.Generic;
using WarCard.Cards;

namespace WarCard
{
    public interface IDeck
    {
        ICardTypeCounts CardTypeCounts { get; }
        int CardsInDeck { get; }

        void AddCard(ICard card);
        void Shuffle();
        ICard DrawCard();
        List<ICard> DrawCards(int quantity);
    }
}