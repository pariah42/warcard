﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarCard.Mechanics;

namespace WCUnitTests
{
    [TestClass]
    public class MechanicsFactoryTest
    {
        #region constants
        private const int ONECOST = 1;
        private const int TWOCOST = 2;
        private const int THREECOST = 3;
        private const int FOURCOST = 4;
        private const int FIVECOST = 5;
        private const int NODAMAGE = 0;
        private const int ONEDAMAGE = 1;
        private const int TWODAMAGE = 2;
        private const int THREEDAMAGE = 3;
        private const int FOURDAMAGE = 4;
        private const int FIVEDAMAGE = 5;
        private const int NOHEAL = 0;
        private const int ONEHEAL = 1;
        private const int NODRAW = 0;
        private const int ONEDRAW = 1;
        private const int NOMANABUFF = 0;
        private const int ONEMANABUFF = 1;
        private const string MESSAGE = "You will never defeat me!";
        #endregion

        [TestMethod]
        public void TestCommonMechanicsFactory()
        {
            IMechanicsFactory factory = new CommonMechanicsFactory();
            Assert.IsNotNull(factory);
            Assert.AreEqual(ONECOST, factory.GetCost());
            Assert.AreEqual(ONEDAMAGE, factory.GetDamage());
            Assert.AreEqual(NOHEAL, factory.GetHeal());
            Assert.AreEqual(NODRAW, factory.GetDraw());
            Assert.AreEqual(NOMANABUFF, factory.GetManaBuff());
            Assert.AreEqual(String.Empty, factory.GetMessage());
        }

        [TestMethod]
        public void TestUncommonMechanicsFactory()
        {
            IMechanicsFactory factory = new UncommonMechanicsFactory();
            Assert.IsNotNull(factory);
            Assert.AreEqual(TWOCOST, factory.GetCost());
            Assert.AreEqual(TWODAMAGE, factory.GetDamage());
            Assert.AreEqual(NOHEAL, factory.GetHeal());
            Assert.AreEqual(NODRAW, factory.GetDraw());
            Assert.AreEqual(NOMANABUFF, factory.GetManaBuff());
            Assert.AreEqual(String.Empty, factory.GetMessage());
        }

        [TestMethod]
        public void TestRareMechanicsFactory()
        {
            IMechanicsFactory factory = new RareMechanicsFactory();
            Assert.IsNotNull(factory);
            Assert.AreEqual(THREECOST, factory.GetCost());
            Assert.AreEqual(THREEDAMAGE, factory.GetDamage());
            Assert.AreEqual(NOHEAL, factory.GetHeal());
            Assert.AreEqual(NODRAW, factory.GetDraw());
            Assert.AreEqual(NOMANABUFF, factory.GetManaBuff());
            Assert.AreEqual(String.Empty, factory.GetMessage());
        }

        [TestMethod]
        public void TestEpicMechanicsFactory()
        {
            IMechanicsFactory factory = new EpicMechanicsFactory();
            Assert.IsNotNull(factory);
            Assert.AreEqual(FOURCOST, factory.GetCost());
            Assert.AreEqual(FOURDAMAGE, factory.GetDamage());
            Assert.AreEqual(NOHEAL, factory.GetHeal());
            Assert.AreEqual(NODRAW, factory.GetDraw());
            Assert.AreEqual(NOMANABUFF, factory.GetManaBuff());
            Assert.AreEqual(String.Empty, factory.GetMessage());
        }

        [TestMethod]
        public void TestTierOneMechanicsFactory()
        {
            IMechanicsFactory factory = new TierOneMechanicsFactory();
            Assert.IsNotNull(factory);
            Assert.AreEqual(FIVECOST, factory.GetCost());
            Assert.AreEqual(FIVEDAMAGE, factory.GetDamage());
            Assert.AreEqual(NOHEAL, factory.GetHeal());
            Assert.AreEqual(NODRAW, factory.GetDraw());
            Assert.AreEqual(NOMANABUFF, factory.GetManaBuff());
            Assert.AreEqual(String.Empty, factory.GetMessage());
        }

        [TestMethod]
        public void TestTierTwoMechanicsFactory()
        {
            IMechanicsFactory factory = new TierTwoMechanicsFactory();
            Assert.IsNotNull(factory);
            Assert.AreEqual(ONECOST, factory.GetCost());
            Assert.AreEqual(NODAMAGE, factory.GetDamage());
            Assert.AreEqual(ONEHEAL, factory.GetHeal());
            Assert.AreEqual(NODRAW, factory.GetDraw());
            Assert.AreEqual(NOMANABUFF, factory.GetManaBuff());
            Assert.AreEqual(String.Empty, factory.GetMessage());
        }

        [TestMethod]
        public void TestTierThreeMechanicsFactory()
        {
            IMechanicsFactory factory = new TierThreeMechanicsFactory();
            Assert.IsNotNull(factory);
            Assert.AreEqual(ONECOST, factory.GetCost());
            Assert.AreEqual(ONEDAMAGE, factory.GetDamage());
            Assert.AreEqual(NOHEAL, factory.GetHeal());
            Assert.AreEqual(ONEDRAW, factory.GetDraw());
            Assert.AreEqual(NOMANABUFF, factory.GetManaBuff());
            Assert.AreEqual(String.Empty, factory.GetMessage());
        }

        [TestMethod]
        public void TestLegendaryMechanicsFactory()
        {
            IMechanicsFactory factory = new LegendaryMechanicsFactory();
            Assert.IsNotNull(factory);
            Assert.AreEqual(FIVECOST, factory.GetCost());
            Assert.AreEqual(FOURDAMAGE, factory.GetDamage());
            Assert.AreEqual(NOHEAL, factory.GetHeal());
            Assert.AreEqual(NODRAW, factory.GetDraw());
            Assert.AreEqual(ONEMANABUFF, factory.GetManaBuff());
            Assert.AreEqual(MESSAGE, factory.GetMessage());
        }
    }
}
