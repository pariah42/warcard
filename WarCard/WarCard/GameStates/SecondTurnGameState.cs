using System;

namespace WarCard.GameStates
{
    public class SecondTurnGameState : GameState
    {
        private bool _cardDrawn;

        public SecondTurnGameState(IGame game) : base(game) { }

        public override void PlayNextState()
        {
            if (_game.PlayerTwo.CardsInDeck == 0)
            {
                Console.WriteLine("{0} you have no cards left in your deck.{1}", _game.PlayerTwo.Name, Environment.NewLine);
                _game.PlayerTwo.TakeEmptyDeckDamage();
                Adjutant.HealthReport(_game.PlayerTwo);
                EndState();
            }

            Adjutant.ManaCrystalReport(_game.PlayerTwo);
            Adjutant.HealthReport(_game.PlayerTwo);
            if (!_cardDrawn)
            {
                _game.PlayerTwo.Draw();
                _cardDrawn = true;
            }
            
            if (!_game.PlayerTwo.HasPlayableCards())
            {
                Adjutant.YoureStuck(_game.PlayerTwo);
                EndState();
            }
            Adjutant.DisplayCards(_game.PlayerTwo.GetCardsInHand());
            var input = Console.ReadLine();
            Adjutant.ExecutePlayerCommand(input, _game.PlayerTwo);
        }

        public override void EndState()
        {
            Adjutant.EndOfTurnMessage(_game.PlayerTwo);
            _cardDrawn = false;
            _game.SetState(_game.GetEndRoundState());
            _game.PlayNextState();
        }
    }
}