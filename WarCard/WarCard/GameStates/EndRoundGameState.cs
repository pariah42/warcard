namespace WarCard.GameStates
{
    public class EndRoundGameState : GameState
    {
        public EndRoundGameState(IGame game) : base(game)
        {
        }

        public override void PlayNextState()
        {
            _game.PlayerOne.AddManaCrystalToTotal();
            _game.PlayerTwo.AddManaCrystalToTotal();
            EndState();
        }

        public override void EndState()
        {
            Adjutant.RoundMessage(_game.Round,false);
            _game.SetState(_game.GetBeginRoundState());
            _game.PlayNextState();
        }
    }
}