using System.Collections.Generic;
using WarCard.Cards;

namespace WarCard
{
    public interface IPlayer
    {
        string Name { get; }
        int CardsInHand { get; }
        int TotalManaCrystalCount { get; }
        int AvailableManaCrystalCount { get; }
        int CardsInDeck { get; }
        int Health { get; }

        bool HasPlayableCards();
        void DrawInitialHand();
        void AddManaCrystalToTotal();
        void ResetAvailableCrystals();
        void Draw();
        ICard GetPlayableCard();
        bool PlayCard(ICard card);
        void TakeDamage(int cardDamage);
        bool IsDead();
        void EndTurn();
        void TakeEmptyDeckDamage();
        List<ICard> GetCardsInHand();
        ICard GetCardNumbered(int index);
    }
}