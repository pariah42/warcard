There are two zip files available for download one is the Release folder(renamed WarCard) with .exe and needed dependencies. The other is a compressed file of the entire source code. Additionally the repository is open to clone and fork.

# Design Decisions #

1. **ORM**: Installed nu-get packages for Dapper.Net and SqLite (which interestingly improts some of EF). I initially started trying to use Entity Framework code-first but I ran into all kinds of problems when I tried to get it to build and run on outside machines. So I scratched it and changed to Dapper. Honestly, I probably could have stayed with EF and figured out what was causing the failures but I wanted to take a look at Dapper anyway. The way I look at it this was an opportunity to embrace the 'Learn and Grow' core value.

2. **State pattern**: I chose to implement the State design pattern to manage game state. The requirements specified anticipation of wanting to tweak card mechanics. If this involves mechanics that occur in phases or response mechanics I believe the IGameState interface will be easy to extend to accommodate that.

3. **Strategy pattern**: ICardTypeCounts is meant to be an implementation of the strategy pattern. It only has one concrete currently so it isn't very interesting yet but it could be extended for new card type distributions (for different deck mechanics).  

4. **Factory pattern**: IMechanicsFactory was implemented to handle change to the mechanics of individual cards. With this factory the Card abstract class could be extended for new mechanics or adjustments to existing mechanics.

5. **Tiered Architecture**: There is an attmept here to keep the application modular. There is a Data Access Layer that could be made more robust for a more advanced data model (for instance CRUD operations for cards, decks, players, etc.) The NPCs are what I would normally call a Service or BusinessLogic class but when I thought about what these types of classes usually do in the context of Blizzard games they seemed to act a lot like an NPC.

## Core Values ##
1. Gameplay First: I did my best to make the I/O functions of the application fun. I would have like to implement more custom messaging for the cards to simulate some of what happens in Hearthstone ("BEHOLD THE MIGHT OF STORMWIND") but I had already spent considerably more time than suggested so I had to sacrifice a little here to make architectural decisions that supported mechanics.
2. Commit to Quality: The majority of the code produced for this application was done using the Test Driven-Development process which is the best way I know to promote quality in code. I ran into some trouble with my GameState tests so they are currently not part of the complete test run.
3. Play nice, Play fair: In reality I am embracing this one somewhat unintentionally. As it will show in my development time report, I spend much longer on this than suggested. This may mean I am disqualified from consideration but being honest and having integrity are my core values and I believe they contribute to the Play fair part of this core value.
4. Embrace your Inner geek: No problem. I used my Blizzard geekiness throughout my code from name choices to code comments. Additionally one of the reasons I took so long on this project was that my inner geek took over. Between the opportunities to implement OO design patterns and the fact that it is a game (which is inherently sooooo cooool) I just kind of went all in.
5. Every voice matters: I am very much looking forward to any feedback I get on this, I have already asked some other developers I know to look at it and tell me what they think(I did this after I was done coding).
6. Think globally: I was hoping to add some kind of language support to put this value into my project. But again, time did not permit me to do all that I had hoped. Perhaps in v2.
7. Lead responsibly: When considering ethics during my time working on this project I had to consider whether to continue on the time intensive path or take short-cuts to adhere to the time requirements. I chose the time intensive path. I believe that it is better to produce a higher quality product and take more time doing it (usually I would communicate this before work began) and no matter how much I want to work at Blizzard, I want to adhere to my beliefs about good software development just as much. I hope my decisions produced a good compromise.
8. Learn and grow: I had many areas of discovery on this project and I am sure I will have many more. I am grateful for getting to work on this because I really did learn a lot both technically and personally.

### Dev Time ###
Saturday: 6 hours
Sunday: 6 hours
Monday: 4 hours 
I know the requirement was 4 hours to a working application and this surely represents a miss of that objective. I believe every commit I did was 'working' in that it built and the tests ran successfully. In a test-driven process I find that the Application layer is last, so I chose instead of having an Application at four hours in to have some solid OO classes and test coverage for them.

I appreciate this opportunity so much. I had great fun, Thank you!