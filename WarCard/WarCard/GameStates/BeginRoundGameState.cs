using System;

namespace WarCard.GameStates
{
    public class BeginRoundGameState : GameState
    {

        public BeginRoundGameState(IGame game) : base(game) { }
        
        public override void PlayNextState()
        {
            _game.PlayerOne.ResetAvailableCrystals();
            _game.PlayerTwo.ResetAvailableCrystals();
            EndState();
        }

        public override void EndState()
        {
            Adjutant.RoundMessage(_game.Round, true);
            _game.SetState(_game.GetFirstTurnState());
            _game.PlayNextState();
        }
    }
}