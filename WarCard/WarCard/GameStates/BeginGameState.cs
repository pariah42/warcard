using System;

namespace WarCard.GameStates
{
    public class BeginGameState : GameState
    {

        public BeginGameState(IGame game) : base(game) { }

        public override void PlayNextState()
        {
            _game.PlayerOne.DrawInitialHand();
            _game.PlayerTwo.DrawInitialHand();
            _game.PlayerOne.AddManaCrystalToTotal();
            _game.PlayerTwo.AddManaCrystalToTotal();
            Adjutant.Introduce(_game.PlayerOne, _game.PlayerTwo);
            var input = Console.ReadLine();
            while (!Adjutant.IsPlayerOnePrepared(input))
            {
                Adjutant.BePatientWithPlayerOne();
                input = Console.ReadLine();
            }
            EndState();
        }

        public override void EndState()
        {
            _game.SetState(_game.GetBeginRoundState());
            _game.PlayNextState();
        }
    }
}