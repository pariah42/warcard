﻿using System;
using DataAccessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WarCard;
using WarCard.Cards;
using WarCard.GameStates;
using WarCard.NPCs;

namespace WCUnitTests
{
    [TestClass]
    public class GameStateTest
    {
        private IDeckNPC _deckNPC;
        private ICardNPC _cardNPC;
        private ICardTypeCounts _cardTypeCounts;
        private Mock<CardRepository> _cardRespository;


        [TestInitialize]
        public void TestInitialize()
        {
            _cardRespository = new Mock<CardRepository>();
            _cardRespository.Setup(repo => repo.GetCards()).Returns(FakeCardRepo.Cards);
            _cardNPC = new CardNPC(_cardRespository.Object);
            _cardTypeCounts = new CardTypeCounts(10, 5, 3, 2, 2, 5, 2, 1);
            _deckNPC = new DeckNPC(_cardNPC, _cardTypeCounts);
        }

        //[TestMethod]
        public void TestBeginGameState()
        {
            IGame game = new Game(_deckNPC);
            game.PlayNextState();
            Assert.AreEqual(game.PlayerOne.CardsInHand, 5);
            Assert.AreEqual(game.PlayerTwo.CardsInHand, 4);
            Assert.AreEqual(game.PlayerOne.TotalManaCrystalCount, 1);
            Assert.AreEqual(game.PlayerTwo.TotalManaCrystalCount, 1);
            Assert.IsTrue(game.GetState() is FirstTurnGameState); //because move from begin game to begin round to first turn is automatic
        }

        //[TestMethod]
        public void TestBeginRoundGameState()
        {
            IGame game = new Game(_deckNPC);
            game.PlayNextState();
            Assert.AreEqual(game.PlayerOne.AvailableManaCrystalCount, game.PlayerOne.TotalManaCrystalCount);
            Assert.AreEqual(game.PlayerTwo.AvailableManaCrystalCount, game.PlayerTwo.TotalManaCrystalCount);
            Assert.IsTrue(game.GetState() is FirstTurnGameState); //because move from begin game to begin round to first turn is automatic
        }

       // [TestMethod]
        public void TestFirstTurnGameState()
        {
            IGame game = new Game(_deckNPC);
            game.PlayNextState();
            Assert.IsTrue(game.GetState() is FirstTurnGameState);
            Assert.AreEqual(game.PlayerOne.CardsInHand, 5);
            Assert.AreEqual(game.PlayerTwo.CardsInHand, 4);
            while (!game.PlayerOne.HasPlayableCards() && game.PlayerOne.CardsInDeck > 0)
            {
                game.PlayerOne.Draw();
            }
            var cardsInHand = game.PlayerOne.CardsInHand;
            var mana = game.PlayerOne.AvailableManaCrystalCount;
            if (!game.PlayerOne.HasPlayableCards())
            {
                Assert.Fail("No cards in deck that cost 1 mana crystal!");
            }
            ICard playableCard = game.PlayerOne.GetPlayableCard();
            Assert.IsNotNull(playableCard);
            game.PlayerOne.PlayCard(playableCard);
            Assert.AreEqual((cardsInHand-1), game.PlayerOne.CardsInHand);
            Assert.AreEqual(mana-playableCard.Cost,game.PlayerOne.AvailableManaCrystalCount);
            if (game.PlayerOne.HasPlayableCards())
            {
                game.PlayerOne.EndTurn();
            }
            Assert.IsTrue(game.GetState() is SecondTurnGameState);
        }

        //[TestMethod]
        public void TestSecondTurnGameState()
        {
            IGame game = new Game(_deckNPC);
            game.PlayNextState();
            while (!game.PlayerOne.HasPlayableCards() && game.PlayerOne.CardsInDeck > 0)
            {
                game.PlayerOne.Draw();
            }
            if (!game.PlayerOne.HasPlayableCards())
            {
                Assert.Fail("No cards in deck that cost 1 mana crystal!");
            }
            ICard playableCard = game.PlayerOne.GetPlayableCard();
            game.PlayerOne.PlayCard(playableCard);
            if (game.PlayerOne.HasPlayableCards())
            {
                game.PlayerOne.EndTurn();
            }
            Assert.IsTrue(game.GetState() is SecondTurnGameState);
            Assert.AreEqual(game.PlayerTwo.CardsInHand, 5);
            while (!game.PlayerTwo.HasPlayableCards() && game.PlayerTwo.CardsInDeck > 0)
            {
                game.PlayerTwo.Draw();
            }
            var cardsInHand = game.PlayerTwo.CardsInHand;
            var mana = game.PlayerTwo.AvailableManaCrystalCount;
            if (!game.PlayerTwo.HasPlayableCards())
            {
                Assert.Fail("No cards in deck that cost 1 mana crystal!");
            }
            playableCard = game.PlayerTwo.GetPlayableCard();
            Assert.IsNotNull(playableCard);
            game.PlayerTwo.PlayCard(playableCard);
            Assert.AreEqual((cardsInHand - 1), game.PlayerTwo.CardsInHand);
            Assert.AreEqual(mana - playableCard.Cost, game.PlayerTwo.AvailableManaCrystalCount);
            if (game.PlayerTwo.HasPlayableCards())
            {
                game.PlayerTwo.EndTurn();
            }
            Assert.IsTrue(game.GetState() is EndRoundGameState);
        }

        //[TestMethod]
        public void TestEndRoundGameState()
        {
            IGame game = new Game(_deckNPC);
            game.PlayNextState();
            while (!game.PlayerOne.HasPlayableCards() && game.PlayerOne.CardsInDeck > 0)
            {
                game.PlayerOne.Draw();
            }
            if (!game.PlayerOne.HasPlayableCards())
            {
                Assert.Fail("No cards in deck that cost 1 mana crystal!");
            }
            ICard playableCard = game.PlayerOne.GetPlayableCard();
            game.PlayerOne.PlayCard(playableCard);
            if (game.PlayerOne.HasPlayableCards())
            {
                game.PlayerOne.EndTurn();
            }
            Assert.IsTrue(game.GetState() is SecondTurnGameState);
            Assert.AreEqual(game.PlayerTwo.CardsInHand, 5);
            while (!game.PlayerTwo.HasPlayableCards() && game.PlayerTwo.CardsInDeck > 0)
            {
                game.PlayerTwo.Draw();
            }
            var cardsInHand = game.PlayerTwo.CardsInHand;
            var mana = game.PlayerTwo.AvailableManaCrystalCount;
            if (!game.PlayerTwo.HasPlayableCards())
            {
                Assert.Fail("No cards in deck that cost 1 mana crystal!");
            }
            playableCard = game.PlayerTwo.GetPlayableCard();
            game.PlayerTwo.PlayCard(playableCard);
            if (game.PlayerTwo.HasPlayableCards())
            {
                game.PlayerTwo.EndTurn();
            }
            Assert.IsTrue(game.GetState() is EndRoundGameState);
            game.PlayNextState();
            Assert.IsTrue(game.GetState() is FirstTurnGameState);
        }
    }
}
