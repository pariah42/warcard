﻿namespace WarCard.Mechanics
{
    public interface IMechanicsFactory
    {
        int GetDamage();
        int GetCost();
        int GetHeal();
        int GetDraw();
        int GetManaBuff();
        string GetMessage();
    }
}
