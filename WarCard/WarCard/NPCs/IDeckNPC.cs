namespace WarCard.NPCs
{
    public interface IDeckNPC
    {
        IDeck GetDeck();
    }
}