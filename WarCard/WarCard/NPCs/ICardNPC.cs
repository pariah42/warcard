using System.Collections.Generic;

namespace WarCard.NPCs
{
    public interface ICardNPC
    {
        void FillDeck(int cardCount, IDeck deck);
    }
}