using System.Collections.Generic;
using WarCard.Cards;

namespace WarCard.NPCs
{
    public class DeckNPC : IDeckNPC
    {
        private readonly ICardNPC _cardNPC;
        private readonly ICardTypeCounts _cardTypeCounts;
        private const int FULLDECK = 30;

        public DeckNPC(ICardNPC cardService, ICardTypeCounts cardTypeCounts)
        {
            _cardNPC = cardService;
            _cardTypeCounts = cardTypeCounts;
        }

        public IDeck GetDeck()
        {
            IDeck deck = new Deck(_cardTypeCounts);
            _cardNPC.FillDeck(FULLDECK, deck);
            return deck;
        }
    }
}