﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using WarCard.Cards;

namespace WarCard
{
    public class Adjutant
    {
        public static void Introduce(IPlayer playerOne, IPlayer playerTwo)
        {
            Console.WriteLine(String.Concat("Welcome to WarCard!", Environment.NewLine));
            Thread.Sleep(500);
            Console.Write(String.Concat("Player One is ", playerOne.Name, "!",  Environment.NewLine));
            Thread.Sleep(500);
            Console.WriteLine(String.Concat("Player Two is ", playerTwo.Name, "!", Environment.NewLine));
            Thread.Sleep(500);
            Console.WriteLine(String.Concat(playerOne.Name, " will have the first turn.", Environment.NewLine));
            Thread.Sleep(500);
            Console.Write(String.Concat(playerOne.Name, " are you prepared for battle?", Environment.NewLine));
        }

        public static bool IsPlayerOnePrepared(string input)
        {
            return input.Trim().ToLower() == "yes";
        }

        public static void BePatientWithPlayerOne()
        {
            Console.Write(String.Concat("Please type 'yes' once you have gathered your courage...", Environment.NewLine));
        }

        public static void DisplayCards(List<ICard> cards)
        {
            Console.Write(String.Concat("Here are your cards...", Environment.NewLine));
            Thread.Sleep(500);
            foreach (ICard card in cards)
            {
                var cardString = String.Format("== {0}. {1} == ||Cost: {2}|| ", (cards.IndexOf(card) + 1), card.Name, card.Cost);
                if (card.Damage != 0)
                {
                    cardString = String.Concat(cardString, String.Format("++Damage: {0}++ ", card.Damage));
                }
                if (card.Heal != 0)
                {
                    cardString = String.Concat(cardString, String.Format("## Heals: {0} ## ", card.Heal));
                }
                if (card.Draw != 0)
                {
                    cardString = String.Concat(cardString, String.Format("You draw {0} ", card.Draw));
                }
                if (card.ManaBuff != 0)
                {
                    cardString = String.Concat(cardString, String.Format(" (+{0} Mana Crystal)", card.ManaBuff));
                }
                Console.Write(String.Concat(cardString,Environment.NewLine));
            }

            Console.Write(String.Concat("Please type the number of the card you wish to play or type 'end' to end your turn.", Environment.NewLine));

        }

        public static void ExecutePlayerCommand(string input, IPlayer player)
        {
            
            bool isValidCommand = false;
            while (!isValidCommand)
            {
                string cleanedInput = input.Trim().ToLower();
                if (cleanedInput == "end")
                {
                    player.EndTurn();
                    return;
                }
                int result;
                //input doesn't make sense, try again
                bool inputParsed = int.TryParse(cleanedInput, out result);
                if (!inputParsed)
                {
                    input = InvalidCommand("You are lacking the sense-making.", player);
                    continue;
                }
                //couldn't find the card, try again
                ICard card = player.GetCardNumbered(result-1);
                if (card == null)
                {
                    input = InvalidCommand("That is not your card you're looking for.", player);
                    continue;
                }

                if (!player.PlayCard(card))
                {
                    input = InvalidCommand("Hmmm... not sure that will work.", player);
                }
                else
                {
                    isValidCommand = true;
                }
            }
            
        }

        private static string InvalidCommand(string message, IPlayer player)
        {
            Console.Write(string.Concat(message, Environment.NewLine));
            Thread.Sleep(500);
            Console.WriteLine(string.Concat("Lets try this again.", Environment.NewLine));
            DisplayCards(player.GetCardsInHand());
            return Console.ReadLine();
        }

        public static void HealthReport(IPlayer player)
        {
            Console.Write("{0} you have {1} health left.{2}", player.Name, player.Health, Environment.NewLine); 
        }

        public static void ManaCrystalReport(IPlayer player)
        {
            Console.Write("{0} you have {1} mana crystals available.{2}", player.Name, player.AvailableManaCrystalCount, Environment.NewLine);
        }

        public static void EndOfTurnMessage(IPlayer player)
        {
            Console.WriteLine("{0} your turn has ended.{1}",player.Name,Environment.NewLine);
            Thread.Sleep(500);
        }

        public static void RoundMessage(int round, bool isBeginning)
        {
            if (isBeginning)
            {
                Console.WriteLine("******************ROUND {0} BEGINS*****************{1}", round, Environment.NewLine);
                Thread.Sleep(500);
            }
            else
            {
                Console.WriteLine("******************ROUND {0} ENDS*****************{1}", round, Environment.NewLine);
                Thread.Sleep(500);
                Console.Clear();
            }

        }

        public static void YoureStuck(IPlayer player)
        {
            Console.Write("{0} you are stuck like chuck. Nothing you can play.{1}",player.Name,Environment.NewLine);
            Thread.Sleep(500);
        }

        public static void ReportOutcome(IPlayer winner, IPlayer loser)
        {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("{0} HAS BEEN DEFEATED!!!!{1}", loser.Name, Environment.NewLine);
            Thread.Sleep(500);
            Console.WriteLine("{0} IS VICTORIOUS!!!!{1}", winner.Name, Environment.NewLine);
            Thread.Sleep(500);
            Console.WriteLine("Thank you for playing WarCard!!!");
            Thread.Sleep(500);
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            Environment.Exit(0);
        }

        public static void BeHealed(int health)
        {
            Console.WriteLine("You are healed for {0}.",health);
        }

        public static void Legendary(string message)
        {
            Console.WriteLine("");
            Console.WriteLine("{0}", message);
            Console.WriteLine();
            Thread.Sleep(700);
        }

        public static void SuperPowers(IPlayer player, int manaBuff)
        {
            Console.WriteLine("{0} has been granted {1} extra mana for this turn.",player.Name,manaBuff);
        }
    }
}