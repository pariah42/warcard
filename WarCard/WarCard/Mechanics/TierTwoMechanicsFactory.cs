﻿using System;

namespace WarCard.Mechanics
{
    public class TierTwoMechanicsFactory : IMechanicsFactory
    {
        public int GetDamage()
        {
            return 0;
        }

        public int GetCost()
        {
            return 1;
        }

        public int GetHeal()
        {
            return 1;
        }

        public int GetDraw()
        {
            return 0;
        }

        public int GetManaBuff()
        {
            return 0;
        }

        public string GetMessage()
        {
            return String.Empty;
        }
    }
}