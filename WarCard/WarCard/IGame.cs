using WarCard.GameStates;

namespace WarCard
{
    public interface IGame
    {
        IPlayer PlayerOne { get; }
        IPlayer PlayerTwo { get; }
        int Round { get; }

        void PlayNextState();
        void EndState();
        IGameState GetState();
        IGameState GetBeginRoundState();
        IGameState GetFirstTurnState();
        IGameState GetSecondTurnState();
        IGameState GetEndRoundState();
        void SetState(IGameState gameState);
        void AssignDamage(int cardDamage, IPlayer fromPlayer);
    }
}