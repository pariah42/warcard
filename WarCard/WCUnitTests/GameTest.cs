﻿using System;
using DataAccessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WarCard;
using WarCard.Cards;
using WarCard.GameStates;
using WarCard.NPCs;

namespace WCUnitTests
{
    [TestClass]
    public class GameTest
    {
        private IDeckNPC _deckNPC;
        private ICardNPC _cardNPC;
        private ICardTypeCounts _cardTypeCounts;
        private Mock<CardRepository> _cardRespository;


        [TestInitialize]
        public void TestInitialize()
        {
            _cardRespository = new Mock<CardRepository>();
            _cardRespository.Setup(repo => repo.GetCards()).Returns(FakeCardRepo.Cards);
            _cardNPC = new CardNPC(_cardRespository.Object);
            _cardTypeCounts = new CardTypeCounts(10, 4, 2, 2, 2, 5, 2, 1);
            _deckNPC = new DeckNPC(_cardNPC, _cardTypeCounts);
        }

        [TestMethod]
        public void TestCreateGame()
        {
            IGame game = new Game(_deckNPC);
            Assert.IsNotNull(game);
        }

        [TestMethod]
        public void TestGameHasPlayers()
        {
            IGame game = new Game(_deckNPC);
            Assert.IsNotNull(game);

            Assert.IsNotNull(game.PlayerOne);
            Assert.IsNotNull(game.PlayerTwo);
            Assert.AreNotSame(game.PlayerOne,game.PlayerTwo);
            Assert.AreNotEqual(game.PlayerOne, game.PlayerTwo);
        }

        //[TestMethod]
        public void TestBeginGame()
        {
            IGame game = new Game(_deckNPC);
            game.PlayNextState();
            Assert.IsNotNull(game.GetState());
            Assert.IsTrue(game.GetState() is FirstTurnGameState); //because move from begin game to begin round to first turn is automatic
            Assert.AreEqual(game.PlayerOne.CardsInHand,5); // because first player draws at beginning of turn
            Assert.AreEqual(game.PlayerTwo.CardsInHand,4);
        }
    }
}
