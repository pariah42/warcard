using System;
using System.Collections.Generic;
using System.Linq;
using WarCard.Cards;
using WarCard.NPCs;

namespace WarCard
{
    public class Player : IPlayer
    {
        private int _totalManaCrystals;
        private int _availableManaCrystals;
        private const int MAXMANACRYSTALS = 10;
        private const int INITIALHANDSIZE = 4;
        private const int STARTINGHEALTH = 30;
        private const int EMPTYDECKDAMAGE = 1;
        private IDeck _deck;
        private IHand _hand;
        private IGame _game;
        private int _hitPoints;

        public Player(string playerName, IDeckNPC deckNPC, IGame game)
        {
            Name = playerName;
            _hand = new Hand(new List<ICard>());
            _deck = deckNPC.GetDeck();
            _deck.Shuffle();
            _game = game;
            _hitPoints = STARTINGHEALTH;
        }

        public string Name { get; private set; }

        public int CardsInHand
        {
            get { return _hand.CardCount; }
        }

        public int TotalManaCrystalCount { get { return _totalManaCrystals; } }
        public int AvailableManaCrystalCount { get { return _availableManaCrystals; } }
        public int CardsInDeck { get { return _deck.CardsInDeck; } }
        public int Health { get { return _hitPoints; } }

        public bool HasPlayableCards()
        {
            return _hand.Cards.Any(card => card.Cost <= _availableManaCrystals);
        }

        public void DrawInitialHand()
        {
            _deck.Shuffle();
            var cards = _deck.DrawCards(INITIALHANDSIZE);
            foreach (var card in cards)
            {
                _hand.AddCard(card);
            }
        }

        public void AddManaCrystalToTotal()
        {
            if (_totalManaCrystals < MAXMANACRYSTALS)
            {
                _totalManaCrystals++;
            }
        }

        public void ResetAvailableCrystals()
        {
            _availableManaCrystals = _totalManaCrystals;
        }

        public void Draw()
        {
            if (_deck.CardsInDeck > 0)
            {
                _hand.AddCard(_deck.DrawCard());
            }
        }

        public void Draw(int quantity)
        {
            List<ICard> cards = _deck.DrawCards(quantity);
            foreach (var card in cards)
            {
                _hand.AddCard(card);
            }
        }

        public ICard GetPlayableCard()
        {
            return HasPlayableCards() ? _hand.Cards.First(card => card.Cost <= _availableManaCrystals) : null;
        }

        public bool PlayCard(ICard card)
        {
            if (!_hand.Cards.Contains(card))
            {
                return false;
            }
            if (card.Cost > _availableManaCrystals)
            {
                return false;
            }
            _game.AssignDamage(card.Damage, this);
            _availableManaCrystals = _availableManaCrystals - card.Cost;
            if (card.Heal > 0)
            {
                _hitPoints += card.Heal;
                Adjutant.BeHealed(card.Heal);
            }
            if (card.ManaBuff > 0)
            {
                Adjutant.Legendary(card.Message);
                _availableManaCrystals += 1;
                Adjutant.SuperPowers(this,card.ManaBuff);
            }
            if (card.Draw > 0)
            {
                Draw(card.Draw);
            }
            _hand.Cards.Remove(card);
            _game.PlayNextState();
            return true;
        }

        public void TakeDamage(int cardDamage)
        {
            _hitPoints = _hitPoints - cardDamage;
        }

        public void TakeEmptyDeckDamage()
        {
            _hitPoints = _hitPoints - EMPTYDECKDAMAGE;
        }

        public List<ICard> GetCardsInHand()
        {
            return new List<ICard>(_hand.Cards);
        }

        public ICard GetCardNumbered(int index)
        {
            return _hand.CardAtIndex(index);
        }

        public bool IsDead()
        {
            return _hitPoints <= 0;
        }

        public void EndTurn()
        {
            _game.EndState();
        }
    }
}