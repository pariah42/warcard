﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarCard;
using WarCard.Cards;
using WarCard.Mechanics;

namespace WCUnitTests
{
    [TestClass]
    public class HandTest
    {
        private static List<ICard> _fourCards; 
            
        [TestInitialize]
        public void TestInitialize()
        {
            IMechanicsFactory factory = new CommonMechanicsFactory();
            _fourCards = new List<ICard>()
            {
                new BasicCard(factory,"name"),
                new BasicCard(factory,"name"),
                new BasicCard(factory,"name"),
                new BasicCard(factory,"name")
            };
        }

        [TestMethod]
        public void TestCreateHand()
        {
            IHand hand = new Hand(_fourCards);
            Assert.IsNotNull(hand);
        }

        [TestMethod]
        public void TestNewHandHasFourCards()
        {
            IHand hand = new Hand(_fourCards);
            Assert.IsNotNull(hand);

            Assert.AreEqual(hand.CardCount,4);
        }

    }
}
