﻿using System;

namespace WarCard.Mechanics
{
    public class TierOneMechanicsFactory : IMechanicsFactory
    {
        public int GetDamage()
        {
            return 5;
        }

        public int GetCost()
        {
            return 5;
        }

        public int GetHeal()
        {
            return 0;
        }

        public int GetDraw()
        {
            return 0;
        }

        public int GetManaBuff()
        {
            return 0;
        }

        public string GetMessage()
        {
            return String.Empty;
        }
    }
}