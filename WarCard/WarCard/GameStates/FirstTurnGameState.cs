using System;

namespace WarCard.GameStates
{
    public class FirstTurnGameState : GameState
    {
        private bool _cardDrawn;

        public FirstTurnGameState(IGame game) : base(game) { }

        public override void PlayNextState()
        {
            if (_game.PlayerOne.CardsInDeck == 0)
            {
                Console.WriteLine("{0} you have no cards left in your deck.{1}", _game.PlayerOne.Name, Environment.NewLine);
                _game.PlayerOne.TakeEmptyDeckDamage();
                Adjutant.HealthReport(_game.PlayerOne);
                EndState();
            }
            Adjutant.ManaCrystalReport(_game.PlayerOne);
            Adjutant.HealthReport(_game.PlayerOne);
            if (!_cardDrawn)
            {
                _game.PlayerOne.Draw();
                _cardDrawn = true;
            }
            
            if (!_game.PlayerOne.HasPlayableCards())
            {
                Adjutant.YoureStuck(_game.PlayerOne);
                EndState();
            }
            Adjutant.DisplayCards(_game.PlayerOne.GetCardsInHand());
            var input = Console.ReadLine();
            Adjutant.ExecutePlayerCommand(input,_game.PlayerOne);
        }

        public override void EndState()
        {
            Adjutant.EndOfTurnMessage(_game.PlayerOne);
            _cardDrawn = false;
            _game.SetState(_game.GetSecondTurnState());
            _game.PlayNextState();
        }
    }
}