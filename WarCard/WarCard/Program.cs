﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using WarCard.Cards;
using WarCard.NPCs;

namespace WarCard
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(100,30);
            ICardNPC cardNPC = new CardNPC(new CardRepository());
            ICardTypeCounts cardTypeCounts = new CardTypeCounts(10,5,3,2,2,5,2,1);
            IDeckNPC deckNPC = new DeckNPC(cardNPC, cardTypeCounts);

            IGame game = new Game(deckNPC);
            game.PlayNextState();
            
        }

    }
}
