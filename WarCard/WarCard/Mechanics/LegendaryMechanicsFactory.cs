﻿namespace WarCard.Mechanics
{
    public class LegendaryMechanicsFactory : IMechanicsFactory
    {
        public int GetDamage()
        {
            return 4;
        }

        public int GetCost()
        {
            return 5;
        }

        public int GetHeal()
        {
            return 0;
        }

        public int GetDraw()
        {
            return 0;
        }

        public int GetManaBuff()
        {
            return 1;
        }

        public string GetMessage()
        {
            return "You will never defeat me!";
        }
    }
}