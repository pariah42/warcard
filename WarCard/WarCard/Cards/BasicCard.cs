using System;
using WarCard.Mechanics;

namespace WarCard.Cards
{
    public class BasicCard : Card
    {
        public BasicCard(IMechanicsFactory factory, string name) : base(factory)
        {
            Name = name;
        }
    }
}