using WarCard.GameStates;
using WarCard.NPCs;

namespace WarCard
{
    public class Game : IGame
    {
        private readonly IDeckNPC _deckNPC;
        private readonly IGameState _beginGameState;
        private readonly IGameState _beginRoundGameState;
        private readonly IGameState _firstTurnGameState;
        private readonly IGameState _secondTurnGameState;
        private readonly IGameState _endRoundGameState;
        private readonly IGameState _endGameState;
        private IGameState _gameState;

        public Game(IDeckNPC deckNPC)
        {
            _deckNPC = deckNPC;
            PlayerOne = new Player("Uther Lightbringer", _deckNPC, this);
            PlayerTwo = new Player("Orgrim Doomhammer", _deckNPC, this);
            _beginGameState = new BeginGameState(this);
            _beginRoundGameState = new BeginRoundGameState(this);
            _firstTurnGameState = new FirstTurnGameState(this);
            _secondTurnGameState = new SecondTurnGameState(this);
            _endRoundGameState = new EndRoundGameState(this);
            _endGameState = new EndGameState(this);
        }

        public IPlayer PlayerOne { get; private set; }
        public IPlayer PlayerTwo { get; private set; }
        public int Round { get; private set; }

        public void PlayNextState()
        {
            if (_gameState == null)
            {
                _gameState = _beginGameState;
                Round = 0;
            }
            if (_gameState == _beginRoundGameState)
            {
                Round = Round + 1;
            }
            _gameState.PlayNextState();
        }

        public void EndState()
        {
            if (_gameState == _endGameState)
            {
                Adjutant.ReportOutcome(PlayerOne, PlayerTwo);
            }
            else
            {
                _gameState.EndState();
            }
        }

        #region State getter/setters
        public IGameState GetState()
        {
            return _gameState;
        }

        public IGameState GetBeginRoundState()
        {
            return _beginRoundGameState;
        }

        public IGameState GetFirstTurnState()
        {
            return _firstTurnGameState;
        }

        public IGameState GetSecondTurnState()
        {
            return _secondTurnGameState;
        }

        public IGameState GetEndRoundState()
        {
            return _endRoundGameState;
        }

        public void SetState(IGameState gameState)
        {
            _gameState = gameState;
        }
        #endregion

        public void AssignDamage(int cardDamage, IPlayer fromPlayer)
        {
            if (PlayerOne == fromPlayer)
            {
                PlayerTwo.TakeDamage(cardDamage);
                if (PlayerTwo.IsDead())
                {
                    SetState(_endGameState);
                    PlayNextState();
                }
            }
            else
            {
                PlayerOne.TakeDamage(cardDamage);
                if (PlayerOne.IsDead())
                {
                    SetState(_endGameState);
                    PlayNextState();
                }
            }
        }

    }
}