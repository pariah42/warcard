﻿using System;
using System.Linq;
using DataAccessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WarCard;
using WarCard.Cards;
using WarCard.NPCs;

namespace WCUnitTests
{
    [TestClass]
    public class CardNPCTest
    {
        private ICardNPC _cardNPC;
        private ICardTypeCounts _cardTypeCounts;
        private Mock<CardRepository> _cardRespository;
        private const int FULLDECK = 30;

        [TestInitialize]
        public void TestInitialize()
        {
            _cardRespository = new Mock<CardRepository>();
            _cardRespository.Setup(repo => repo.GetCards()).Returns(FakeCardRepo.Cards);
            _cardNPC = new CardNPC(_cardRespository.Object);
            _cardTypeCounts = new CardTypeCounts(10, 5, 3, 2, 2, 5, 2, 1);
        }

        [TestMethod]
        public void TestCreateCardNPC()
        {
            Assert.IsNotNull(_cardNPC);
        }

        [TestMethod]
        public void TestFillDeck()
        {
            IDeck deck = new Deck(_cardTypeCounts);
            _cardNPC.FillDeck(FULLDECK, deck);
            Assert.AreEqual(FULLDECK, deck.CardsInDeck);
        }
    }
}
