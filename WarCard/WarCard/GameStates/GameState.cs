namespace WarCard.GameStates
{
    public abstract class GameState : IGameState
    {
        protected IGame _game;

        protected GameState(IGame game)
        {
            _game = game;
        }

        public IPlayer PlayerOne
        {
            get { return _game.PlayerOne; }
        }

        public IPlayer PlayerTwo
        {
            get { return _game.PlayerTwo; }
        }

        public abstract void PlayNextState();
        public abstract void EndState();

    }
}