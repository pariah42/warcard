﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarCard;
using WarCard.Cards;

namespace WCUnitTests
{
    [TestClass]
    public class DeckTest
    {
       
        private ICardTypeCounts _cardTypeCounts;

        [TestInitialize]
        public void TestInitialize()
        {
            _cardTypeCounts = new CardTypeCounts(10, 4, 2, 2, 2, 5, 2, 1);
        }

        [TestMethod]
        public void TestCreateDeck()
        {
            IDeck deck = new Deck(_cardTypeCounts);
            Assert.IsNotNull(deck);
        }
        
    }
}
