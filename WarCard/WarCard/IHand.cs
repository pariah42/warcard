using System.Collections.Generic;
using WarCard.Cards;

namespace WarCard
{
    public interface IHand
    {
        int CardCount { get; }
        List<ICard> Cards { get; }
        void AddCard(ICard card);
        ICard CardAtIndex(int index);
    }
}