﻿namespace DataAccessLayer
{
    public class Card
    {
        public int CardId { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
    }
}