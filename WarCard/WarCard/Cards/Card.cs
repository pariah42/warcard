using System;
using System.Globalization;
using WarCard.Mechanics;

namespace WarCard.Cards
{
    public abstract class Card : ICard
    {
        private readonly IMechanicsFactory _mechanicsFactory;

        protected Card(IMechanicsFactory mechanicsFactory)
        {
            _mechanicsFactory = mechanicsFactory;
        }

        public string Name { get; protected set; }
        public virtual int Cost { get { return _mechanicsFactory.GetCost(); } }
        public virtual int Damage { get { return _mechanicsFactory.GetDamage(); } }
        public virtual int Heal { get { return _mechanicsFactory.GetHeal(); } }
        public virtual int Draw { get { return _mechanicsFactory.GetDraw(); } }
        public virtual int ManaBuff { get { return _mechanicsFactory.GetManaBuff(); } }
        public virtual string Message { get { return _mechanicsFactory.GetMessage(); } }

        public bool IsEqual(ICard card)
        {
            return String.Compare(card.Name, Name,CultureInfo.CurrentCulture,CompareOptions.None) == 0;
        }
    }
}