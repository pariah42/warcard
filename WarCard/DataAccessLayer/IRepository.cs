﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataAccessLayer
{
    public interface ICardRepository 
    {
        //Only putting what I need, this would normally have much more in it
        Card GetCard(int id);
        List<Card> GetCards();
    }
}
