using System;

namespace WarCard.Cards
{
    public interface ICard
    {
        string Name { get; }
        int Cost { get; }
        int Damage { get; }
        int Heal { get; }
        int Draw { get; }
        int ManaBuff { get; }
        string Message { get; }

        bool IsEqual(ICard card);
    }
}