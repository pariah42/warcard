namespace WarCard.Cards
{
    public class CardTypeCounts : ICardTypeCounts
    {
        public CardTypeCounts(int common, int uncommon, int rare, int epic, int tierOne, int tierTwo, int tierThree, int legendary)
        {
            Legendary = legendary;
            TierThree = tierThree;
            TierTwo = tierTwo;
            TierOne = tierOne;
            Epic = epic;
            Rare = rare;
            Uncommon = uncommon;
            Common = common;
        }

        public int Common { get; private set; }
        public int Uncommon { get; private set; }
        public int Rare { get; private set; }
        public int Epic { get; private set; }
        public int TierOne { get; private set; }
        public int TierTwo { get; private set; }
        public int TierThree { get; private set; }
        public int Legendary { get; private set; }

        public int Total {get { return (Common + Uncommon + Rare + Epic + TierOne + TierTwo + TierThree + Legendary); }}
    }
}