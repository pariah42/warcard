﻿using System;

namespace WarCard.Mechanics
{
    public class UncommonMechanicsFactory : IMechanicsFactory
    {
        public int GetDamage()
        {
            return 2;
        }

        public int GetCost()
        {
            return 2;
        }

        public int GetHeal()
        {
            return 0;
        }

        public int GetDraw()
        {
            return 0;
        }

        public int GetManaBuff()
        {
            return 0;
        }

        public string GetMessage()
        {
            return String.Empty;
        }
    }
}