namespace WarCard.Cards
{
    public interface ICardTypeCounts
    {
        int Common { get; }
        int Uncommon { get; }
        int Rare { get; }
        int Epic { get; }
        int TierOne { get; }
        int TierTwo { get; }
        int TierThree { get; }
        int Legendary { get; }
        int Total { get; }
    }
}