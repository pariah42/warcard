﻿using System;

namespace WarCard.Mechanics
{
    public class RareMechanicsFactory : IMechanicsFactory
    {
        public int GetDamage()
        {
            return 3;
        }

        public int GetCost()
        {
            return 3;
        }

        public int GetHeal()
        {
            return 0;
        }

        public int GetDraw()
        {
            return 0;
        }

        public int GetManaBuff()
        {
            return 0;
        }

        public string GetMessage()
        {
            return String.Empty;
        }
    }
}