﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarCard;
using WarCard.Cards;
using WarCard.Mechanics;

namespace WCUnitTests
{
    [TestClass]
    public class CardTest
    {

        [TestMethod]
        public void TestBasicCard()
        {
            IMechanicsFactory factory = new CommonMechanicsFactory();
            ICard card = new BasicCard(factory, "Angry Chicken");
            Assert.IsNotNull(card);
        }

        

    }
}
