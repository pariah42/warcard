﻿using System;
using DataAccessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WarCard;
using WarCard.Cards;
using WarCard.NPCs;

namespace WCUnitTests
{
    [TestClass]
    public class PlayerTest
    {
        private IDeckNPC _deckNPC;
        private ICardNPC _cardNPC;
        private ICardTypeCounts _cardTypeCounts;
        private Mock<CardRepository> _mockCardRespository;
        private Mock<IGame> _mockedGame;

        private const int INITIALHANDSIZE = 4;
        private const int DECKSIZE = 30;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockCardRespository = new Mock<CardRepository>();
            _mockCardRespository.Setup(repo => repo.GetCards()).Returns(FakeCardRepo.Cards);
            _cardNPC = new CardNPC(_mockCardRespository.Object);
            _cardTypeCounts = new CardTypeCounts(10, 5, 3, 2, 2, 5, 2, 1);

            _deckNPC = new DeckNPC(_cardNPC,_cardTypeCounts);
            _mockedGame = new Mock<IGame>();
        }

        [TestMethod]
        public void TestPlayerCreate()
        {
            IGame game = new Game(_deckNPC);
            IPlayer player = new Player("Uther Lightbringer", _deckNPC, _mockedGame.Object);
            Assert.IsNotNull(player);
        }

        [TestMethod]
        public void TestPlayerName()
        {
            IPlayer player = new Player("Uther Lightbringer", _deckNPC, _mockedGame.Object);
            Assert.AreEqual(player.Name,"Uther Lightbringer");
        }

        [TestMethod]
        public void TestDrawInitialHand()
        {
            IPlayer player = new Player("Uther Lightbringer", _deckNPC, _mockedGame.Object);
            player.DrawInitialHand();
            Assert.AreEqual(player.CardsInHand, INITIALHANDSIZE);
            Assert.AreEqual(player.CardsInDeck, DECKSIZE - INITIALHANDSIZE);
        }

        [TestMethod]
        public void TestDraw()
        {
            IPlayer player = new Player("Uther Lightbringer", _deckNPC, _mockedGame.Object);
            player.DrawInitialHand();
            Assert.AreEqual(player.CardsInHand, INITIALHANDSIZE);
            Assert.AreEqual(player.CardsInDeck, DECKSIZE - INITIALHANDSIZE);
            player.Draw();
            Assert.AreEqual(player.CardsInHand, INITIALHANDSIZE+1);
            Assert.AreEqual(player.CardsInDeck, DECKSIZE - (INITIALHANDSIZE+1));
        }
    }
}
