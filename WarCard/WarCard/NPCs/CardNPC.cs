using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer;
using WarCard.Cards;
using WarCard.Mechanics;
using Card = DataAccessLayer.Card;

namespace WarCard.NPCs
{
    public class CardNPC : ICardNPC
    {
        private readonly CardRepository _cardRepository;

        public CardNPC(CardRepository cardRepository)
        {
            _cardRepository = cardRepository;
        }

        public void FillDeck(int cardCount, IDeck deck)
        {
            IEnumerable<ICard> cards = GetCards(deck.CardTypeCounts);
            foreach (ICard card in cards)
            {
                deck.AddCard(card);
            }
        }

        private IEnumerable<ICard> GetCards(ICardTypeCounts cardTypeCounts)
        {
            var cards = new List<ICard>();
            IMechanicsFactory factory = new CommonMechanicsFactory();
            cards.AddRange(GetDistinctCards(cardTypeCounts.Common,factory));
            factory = new UncommonMechanicsFactory();
            cards.AddRange(GetDistinctCards(cardTypeCounts.Uncommon, factory));
            factory = new RareMechanicsFactory();
            cards.AddRange(GetDistinctCards(cardTypeCounts.Epic, factory));
            factory = new EpicMechanicsFactory();
            cards.AddRange(GetDistinctCards(cardTypeCounts.Rare, factory));
            factory = new TierOneMechanicsFactory();
            cards.AddRange(GetDistinctCards(cardTypeCounts.TierOne, factory));
            factory = new TierTwoMechanicsFactory();
            cards.AddRange(GetDistinctCards(cardTypeCounts.TierTwo, factory));
            factory = new TierThreeMechanicsFactory();
            cards.AddRange(GetDistinctCards(cardTypeCounts.TierThree, factory));
            factory = new LegendaryMechanicsFactory();
            cards.AddRange(GetDistinctCards(cardTypeCounts.Legendary, factory));
            return cards;
        }

        private List<ICard> GetDistinctCards(int quantity, IMechanicsFactory factory)
        {
            string type = GetCardType(factory);
            List<DataAccessLayer.Card> dbCards = _cardRepository.GetCards();

            //coin flip
            if (DateTime.Now.Second%2 == 0)
            {
                dbCards = (from c in dbCards
                           where c.CardType == type
                           select c).OrderByDescending(o => o.CardId).Take(quantity).Distinct().ToList();
            }
            else
            {
                dbCards = (from c in dbCards
                           where c.CardType == type
                           select c).OrderBy(o=>o.CardId).Take(quantity).Distinct().ToList();
            }

            List<ICard> cards = new List<ICard>();
            foreach (var card in dbCards)
            {
                cards.Add(new BasicCard(factory,card.CardName));
            }
            return cards;
        }

        private string GetCardType(IMechanicsFactory factory)
        {
            if (factory is CommonMechanicsFactory)
            {
                return "Common";
            }
            else if (factory is UncommonMechanicsFactory)
            {
                return "Uncommon";
            }
            else if (factory is RareMechanicsFactory)
            {
                return "Rare";
            }
            else if (factory is EpicMechanicsFactory)
            {
                return "Epic";
            }
            else if (factory is TierOneMechanicsFactory)
            {
                return "Tier1";
            }
            else if (factory is TierTwoMechanicsFactory)
            {
                return "Tier2";
            }
            else if (factory is TierThreeMechanicsFactory)
            {
                return "Tier3";
            }
            else if(factory is LegendaryMechanicsFactory)
            {
                return "Legendary";
            }
            return "";
        }
    }
}