namespace WarCard.GameStates
{
    public class EndGameState : GameState
    {
        public EndGameState(IGame game) : base(game) { }

        public override void PlayNextState()
        {
            EndState();
        }

        public override void EndState()
        {
            _game.EndState();
        }
    }
}