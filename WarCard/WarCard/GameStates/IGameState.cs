namespace WarCard.GameStates
{
    public interface IGameState
    {
        IPlayer PlayerOne { get; }
        IPlayer PlayerTwo { get; }
        void PlayNextState();
        void EndState();
    }
}