﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WarCard;
using WarCard.Cards;
using WarCard.NPCs;

namespace WCUnitTests
{
    [TestClass]
    public class DeckNPCTest
    {
        private IDeckNPC _deckNPC;
        private ICardNPC _cardNPC;
        private ICardTypeCounts _cardTypeCounts;
        private Mock<CardRepository> _cardRespository;

        private const int FULLDECK = 30;

        [TestInitialize]
        public void TestInitialize()
        {
            _cardRespository = new Mock<CardRepository>();
            _cardRespository.Setup(repo => repo.GetCards()).Returns(FakeCardRepo.Cards);
            _cardNPC = new CardNPC(_cardRespository.Object);
            _cardTypeCounts = new CardTypeCounts(10, 5, 3, 2, 2, 5, 2, 1);
            _deckNPC = new DeckNPC(_cardNPC,_cardTypeCounts);
        }

        [TestMethod]
        public void TestCreateDeckService()
        {
            Assert.IsNotNull(_deckNPC);
        }

        [TestMethod]
        public void TestGetDeck()
        {
            IDeck deck = _deckNPC.GetDeck();
            Assert.IsNotNull(deck);

            //assert.IsPlayingWithAFullDeck
            Assert.AreEqual(deck.CardsInDeck,FULLDECK);
        }

        [TestMethod]
        public void TestShuffle()
        {
            IDeck deck = _deckNPC.GetDeck();
            IDeck shuffledDeck = _deckNPC.GetDeck();
            shuffledDeck.Shuffle();
            Assert.AreNotSame(deck,shuffledDeck);
            Assert.AreNotEqual(deck.DrawCard().Name,shuffledDeck.DrawCard().Name);
        }
    }
}
